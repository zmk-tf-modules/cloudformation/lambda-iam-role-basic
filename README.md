# Lambda Function Basic IAM Role

This repository contains a CloudFormation Template for an IAM role that can be used as a basis for Lambda Functions.

It was written initially for the **Serverless Bank** learning project and blog series (https://medium.com/@zhangmake86/the-serverless-bank-pt1-e66b2867a177)

It is intended to be used as a CloudFormation Module.

A full explanation of CloudFormation Modules, including how to create and use one can be found here: https://aws.amazon.com/blogs/apn/using-aws-cloudformation-modules-to-improve-enterprise-security/

